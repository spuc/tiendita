﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace tienda
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute(
                "Paises NorteAmerica",
                "Paises/{pais}",
                new { controller = "Paises", action = "NorteAmerica" },
                new { pais = "(us|ca|mx)" }
                );

            routes.MapRoute(
               "Paises Europeos",
               "Paises/{pais}",
               new { controller = "Paises", action = "Europa" },
               new { pais = "(uk|de|it|fr|be)" }
               );

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}