﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace tienda.Controllers
{
    public class PaisesController : Controller
    {
        //
        // GET: /Paises/

        [Authorize()]
        public ActionResult Index()

        {
            var p = Request.QueryString["pais"];
            ViewBag.Mensaje = "Pais: " + p;
            return View();
        }

        //
        // GET: /Paises/Details/5

        public ActionResult NorteAmerica(string pais)
        {
            return View();
        }

        public ActionResult Europa(string pais)
        {
            return View();
        }

        //
        // GET: /Paises/Create

        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /Paises/Create

        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        //
        // GET: /Paises/Edit/5

        public ActionResult Edit(int id)
        {
            return View();
        }

        //
        // POST: /Paises/Edit/5

        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        //
        // GET: /Paises/Delete/5

        public ActionResult Delete(int id)
        {
            return View();
        }

        //
        // POST: /Paises/Delete/5

        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
